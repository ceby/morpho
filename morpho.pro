TEMPLATE = app
INCLUDEPATH += src
CONFIG += release warn_on embed_manifest_exe qt
QT += network webkit

DEFINES += MORPHO_VERSION=\\\"0.4.0\\\"

MOC_DIR = .moc
OBJECTS_DIR = .obj
RCC_DIR = .res

SOURCES += \
	src/util.cpp \
	src/main.cpp \
	src/tabbar.cpp \
	src/webpage.cpp \
	src/webview.cpp \
	src/settings.cpp \
	src/authdialog.cpp \
	src/downloader.cpp \
	src/mainwindow.cpp

HEADERS += \
	src/util.h \
	src/tabbar.h \
	src/webpage.h \
	src/webview.h \
	src/settings.h \
	src/authdialog.h \
	src/downloader.h \
	src/mainwindow.h \
	src/defaultconfig.h

unix {
	QT += dbus
	SOURCES += src/dbus.cpp
	HEADERS += src/dbus.h
	DEFINES += "\"OS=\\\"`uname -sm`\\\"\""
	DEFINES += DBUS
}

