/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "tabbar.h"

#include "downloader.h"
#include "mainwindow.h"
#include <qmenu.h>
#include <qstack.h>
#include <qwebframe.h>
#include "settings.h"
#include "webpage.h"

// TODO: should we limit the size of the tab stack?

TabBar::TabBar(QObject *parent)
	: QObject(parent)
	, _tabs(new QList<WebPage*>)
	, _currentTab(NULL)
	, _currentIndex(-1)
	, _tabStack(new QStack<WebPage*>)
	, _trashMenu(new QMenu(tr("Closed Tabs")))
{
}

void TabBar::setCurrent(WebPage *tab, int i, bool calledWhileRemoving)
{
	if (tab != currentTab()) {
		if (currentTab() && !calledWhileRemoving)
			_tabStack->push(currentTab());

		_currentTab = tab;
		_currentIndex = i;

		emit currentChanged(tab, i);
	}
}

void TabBar::setCurrent(WebPage *tab, bool calledWhileRemoving)
{
	setCurrent(tab, _tabs->indexOf(tab), calledWhileRemoving);
}

void TabBar::setCurrentIndex(int i)
{
	if (i < _tabs->count() && i >= 0)
		setCurrent(_tabs->at(i), i, false);
}

void TabBar::load(const QUrl &url)
{
	WebPage *tab = addTab();
	settings->mainWindow()->activateWindow();
	tab->mainFrame()->load(url);
}

WebPage *TabBar::addTab(bool focus)
{
	WebPage *tab = new WebPage(this);
	tab->setForwardUnsupportedContent(true);

	_tabs->append(tab);

	connect(tab, SIGNAL(unsupportedContent(QNetworkReply*)),
	        settings->downloader(), SLOT(downloadPrompt(QNetworkReply*)));
	connect(tab, SIGNAL(downloadRequested(const QNetworkRequest&)),
	        settings->downloader(), SLOT(download(const QNetworkRequest&)));

	tab->setNetworkAccessManager(settings->networkManager());

	emit tabCountChanged(_tabs->count());

	if (focus)
		setCurrent(tab);

	return tab;
}

WebPage *TabBar::addTab(const QString &url, bool focus)
{
	WebPage *tab = addTab(focus);
	tab->open(url);
	return tab;
}

void TabBar::removeTab(WebPage* tab)
{
	// this seems sort of wasteful...
	int i = 0;
	while ((i = _tabStack->indexOf(tab, i)) != -1)
		_tabStack->remove(i);

	if (_tabs->count() == 1)
		setCurrent(NULL, -1, true);
	else if (currentTab() == tab)
		setCurrent(_tabStack->pop(), true);

	if (_trashMenu->actions().count() == 10)
		_trashMenu->removeAction(_trashMenu->actions().at(0));

	QAction *trashPage = _trashMenu->addAction(QIcon(tab->mainFrame()->icon()),
		tab->titleOrUrl(), this, SLOT(respondToAction()));
	trashPage->setStatusTip(tab->mainFrame()->url().toString());

	_tabs->removeOne(tab);
	delete tab;

	emit tabCountChanged(_tabs->count());
}

void TabBar::removeTab()
{
	if (_currentTab)
		removeTab(_currentTab);
}

void TabBar::respondToAction()
{
	QAction *sending = static_cast<QAction*>(sender());
	addTab(sending->statusTip());
	_trashMenu->removeAction(sending);
}
