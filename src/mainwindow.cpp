/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "mainwindow.h"

#include <qapplication.h>
#include <qboxlayout.h>
#include <qclipboard.h>
#include <qdir.h>
#include <qdockwidget.h>
#include <qevent.h>
#include <qfiledialog.h>
#include <qlineedit.h>
#include <qlinkedlist.h>
#include <qpushbutton.h>
#include <qtoolbar.h>
#include <qwebframe.h>
#include "settings.h"
#include "tabbar.h"
#include "webpage.h"
#include "webview.h"

#ifdef DBUS
#include "dbus.h"
#endif

int minHeight = -1;

QLineEdit *statusBox(bool readOnly = false)
{
	QLineEdit *status = new QLineEdit;

	if (minHeight == -1)
		minHeight = QFontMetrics(status->font()).height();

	status->setFrame(false);
	status->setFixedHeight(minHeight);
	if (readOnly)
		status->setReadOnly(true);

	QPalette p = status->palette();
	QColor color = p.color(QPalette::Normal, QPalette::Window);
	p.setColor(QPalette::Normal, QPalette::Base, color);
	p.setColor(QPalette::Inactive, QPalette::Base, color);
	status->setPalette(p);

	return status;
}

MainWindow::MainWindow(const QUrl &url)
	: _view(new WebView(this))
	, _tabs(new TabBar)
	, _dock(NULL)
	, _insertMode(false)
{
	QToolBar *bar = new QToolBar(tr("Command Box"));
	bar->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
	_commandLine = statusBox();
	bar->addWidget(_commandLine);
	addToolBar(Qt::BottomToolBarArea, bar);

	addToolBarBreak(Qt::BottomToolBarArea);

	_statusBar = new QToolBar(tr("Status"));
	_statusBar->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
	_statusBarLeft = statusBox(true);
	_statusBar->addWidget(_statusBarLeft);
	_statusBarRight = statusBox(true);
	_statusBarRight->setAlignment(Qt::AlignRight);
	_statusBar->addWidget(_statusBarRight);
	addToolBar(Qt::BottomToolBarArea, _statusBar);

	setCentralWidget(_view);

	connect(_view, SIGNAL(titleChanged(const QString&)),
	        this, SLOT(setWindowTitle(const QString&)));
	connect(_view, SIGNAL(urlChanged(const QUrl&)),
	        this, SLOT(changeUrl(const QUrl&)));
	connect(_view, SIGNAL(statusBarMessage(const QString&)),
	        this, SLOT(setStatusBarText(const QString&)));

	connect(_commandLine, SIGNAL(returnPressed()),
	        this, SLOT(commandEntered()));

	connect(_tabs, SIGNAL(currentChanged(WebPage*, int)),
	        this, SLOT(setCurrent(WebPage*, int)));
	connect(_tabs, SIGNAL(tabCountChanged(int)),
	        this, SLOT(tabCountChanged(int)));

#ifdef DBUS
	new DBusAdaptor(this);
#endif

	_view->setFocus();

	QString statusFormat = settings->statusFormat();
	_statusTextLeft = new QLinkedList<QString*>;
	_statusTextRight = new QLinkedList<QString*>;

	QLinkedList<QString*> *status = _statusTextLeft;
	QString *item;
	int i = -1, lastI = -2;
	while ((i = statusFormat.indexOf('%', i+1)) != -1) {
		item = new QString(statusFormat.mid(lastI + 2, i - lastI - 2));
		if (item->isEmpty())
			delete item;
		else
			status->append(item);

		switch (statusFormat[i+1].toAscii()) {
		case '>':
			status = _statusTextRight;
			lastI = i;
			continue;
		case 'U': item = &_hoverLink; break;
		case 'u': item = &_url; break;
		case 's': item = &_statusBarText; break;
		case 't': item = &_currentIndex; break;
		case 'T': item = &_tabCount; break;
		case 'M': item = &_mode; break;
		}
		status->append(item);

		lastI = i;
	}
	if (lastI + 2 < statusFormat.length())
		status->append(new QString(statusFormat.mid(lastI + 2)));

	_tabs->addTab()->mainFrame()->load(url);
}

void MainWindow::setStatusBarText(const QString &v)
{
	_statusBarText = v;
	updateStatusBar();
}

void MainWindow::setLinkText(const QString &url, const QString&, const QString&)
{
	_hoverLink = url.isEmpty() ? _url : tr("Link: ") + url;
	updateStatusBar();
}

void MainWindow::changeUrl(const QUrl &url)
{
	_hoverLink = _url = url.toString();
	updateStatusBar();
}

void MainWindow::tabCountChanged(int count)
{
	_tabCount = QString::number(count);
	updateStatusBar();
}

void MainWindow::updateStatusBar()
{
	QString temp;

	foreach(QString *i, *_statusTextLeft)
		temp += *i;
	_statusBarLeft->setText(temp);

	temp.clear();

	foreach(QString *i, *_statusTextRight)
		temp += *i;
	_statusBarRight->setText(temp);
}

void MainWindow::setCurrent(WebPage *page, int i)
{
	_view->page()->disconnect(this);
	_view->setPage(page);

	if (!page) {
		_view->page(); // create a new QWebPage
	} else {
		connect(page, SIGNAL(linkHovered(const QString, const QString&, const QString&)),
		        this, SLOT(setLinkText(const QString&, const QString&, const QString&)));
		setWindowTitle(page->titleOrUrl());
	}

	_currentIndex = QString::number(i + 1);
	changeUrl(page ? _view->url() : QUrl("about:blank"));
}

void MainWindow::openFiles()
{
	QStringList pages = QFileDialog::getOpenFileNames(this, tr("Open Local Page"), QDir::homePath());
	foreach (QString page, pages)
		_tabs->addTab("file://" + page);
}

void MainWindow::saveSessionAndClose()
{
	// TODO: save session
	qApp->closeAllWindows();
}

void MainWindow::showCommandOutput(QWidget *widget)
{
	_dock = new QDockWidget(tr("Command Output"));
	_dock->setFeatures(QDockWidget::NoDockWidgetFeatures);
	_dock->setWidget(widget);
	addDockWidget(Qt::BottomDockWidgetArea, _dock, Qt::Horizontal);
}

void MainWindow::showTabs()
{
	QVBoxLayout *layout = new QVBoxLayout;
	QPushButton *btn;
	for (QList<WebPage*>::const_iterator tab = _tabs->tabs()->constBegin(),
			end = _tabs->tabs()->constEnd(); tab != end; ++tab) {
		btn = new QPushButton((*tab)->mainFrame()->icon(), (*tab)->titleOrUrl());
		btn->setFlat(true);
		btn->setCheckable(false);
		connect(btn, SIGNAL(pressed()),
		        *tab, SLOT(raiseSelf()));
		layout->addWidget(btn);
	}

	QWidget *dispWid = new QWidget;
	dispWid->setLayout(layout);
	showCommandOutput(dispWid);
}

void MainWindow::runCommand(const QString &str)
{
	QString cmd = str.section(' ', 0, 0);
	QString args = str.section(' ', 1);

	if (cmd == "back")
		_view->back();
	else if (cmd == "forward")
		_view->forward();
	else if (cmd == "buffer")
		_tabs->setCurrentIndex(args.toInt() - 1);
	else if (cmd == "tabopen")
		_tabs->addTab(args);
	else if (cmd == "clipboardopen")
		_tabs->addTab(QApplication::clipboard()->text());
	else if (cmd == "copyurl")
		QApplication::clipboard()->setText(_view->url().toString());
#ifdef Q_WS_X11
	else if (cmd == "selectionopen")
		_tabs->addTab(QApplication::clipboard()->text(QClipboard::Selection));
	else if (cmd == "copyurltoselection")
		QApplication::clipboard()->setText(_view->url().toString(), QClipboard::Selection);
#endif
	else if (cmd == "insert")
		toggleInsertMode(args.isEmpty() ? -1 : args.toInt());
	else if (cmd == "open") {
		WebPage *tab = _tabs->currentTab();
		if (tab)
			tab->open(args);
		else
			_tabs->addTab(args);
	} else if (cmd == "open!")
		openFiles();
	else if (cmd == "ls")
		showTabs();
	else if (cmd == "addtab" || cmd == "tabadd")
		_tabs->addTab(QString());
	else if (cmd == "tabclose")
		_tabs->removeTab();
	else if (cmd == "quit")
		saveSessionAndClose();
	else if (cmd == "quit!")
		qApp->closeAllWindows();
	else if (cmd == "reload")
		_view->reload();
	else if (cmd == "stop")
		_view->stop();
}

void MainWindow::toggleInsertMode(int on)
{
	_insertMode = on == -1 ? !_insertMode : on != 0;
	_mode = on ? tr("-- INSERT --") : "";
	updateStatusBar();
}

bool MainWindow::willNotHandleKey(int key)
{
	return _insertMode || key == Qt::Key_Up || key == Qt::Key_Down || key == Qt::Key_Left
		|| key == Qt::Key_Right || key == Qt::Key_PageDown || key == Qt::Key_PageUp;
}

void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
	if (_dock && ev->key() < 0x01000000) { // ensure it's just a modifier
		_dock->deleteLater();
		_dock = NULL;
	}

	if (ev->key() == Qt::Key_Escape) {
		if (_insertMode)
			toggleInsertMode(false);
		else {
			_commandLine->clear();
			_view->setFocus();
		}
	}

	ev->ignore();
}

void MainWindow::handleKeyReleaseEvent(int key)
{
	switch (key) {
	case Qt::ShiftModifier + Qt::Key_Colon:
		_commandLine->setText(":");
		_commandLine->setFocus();
		break;
	default:
		QString command = settings->keybind(key);
		if (!command.isEmpty()) {
			if (command[0] == '%') {
				_commandLine->setText(':' + command.mid(1) + ' ');
				_commandLine->setFocus();
			} else
				runCommand(command);
		}
	}
}

void MainWindow::commandEntered()
{
	QString command = _commandLine->text();

	if (command[0] == ':') {
		runCommand(command.mid(1));
	} else if (command[0] == '/') {
		// TODO: find in page
	}

	_view->setFocus();
	_commandLine->clear();
}
