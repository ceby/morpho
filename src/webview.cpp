/*
 * Copyright 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "webview.h"

#include "mainwindow.h"
#include <qapplication.h>
#include <qevent.h>

WebView::WebView(MainWindow *parent)
	: QWebView(parent)
	, _mainWindow(parent)
{
}

void WebView::keyPressEvent(QKeyEvent *ev)
{
	if (_mainWindow->willNotHandleKey(ev->modifiers() + ev->key()))
		QWebView::keyPressEvent(ev);
}

void WebView::keyReleaseEvent(QKeyEvent *ev)
{
	int key = ev->modifiers() + ev->key();
	if (_mainWindow->willNotHandleKey(key))
		QWebView::keyReleaseEvent(ev);
	else
		_mainWindow->handleKeyReleaseEvent(key);
}
