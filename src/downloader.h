/*
 * Copyright 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <qdialog.h>

class QLabel;
class QLineEdit;
class QNetworkReply;
class QNetworkRequest;
class QTabWidget;

class DownloadDialog : public QDialog {
	Q_OBJECT
public:
	DownloadDialog(QWidget *parent);
	void focusDownload();
	bool promptDownload(const QString&, QString*, QString*);
protected slots:
	void selectFile();
private:
	QLineEdit *_filePath, *_handler;
	QLabel *_mimeType;
	QString _fileName;
	QTabWidget *_tabs;
};

class Downloader : public QObject {
	Q_OBJECT
public:
	Downloader(QObject *parent = NULL);
public slots:
	void download(const QNetworkRequest&);
	void downloadPrompt(QNetworkReply *reply = NULL);
};

#endif
