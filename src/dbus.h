/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef DBUS_H
#define DBUS_H

#include <qdbusabstractadaptor.h>

class MainWindow;

class DBusAdaptor : public QDBusAbstractAdaptor {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.morpho.browser")
public:
	DBusAdaptor(MainWindow *obj);
public slots:
	Q_NOREPLY void quit();
	Q_NOREPLY void openUrl(const QString &url);
private:
	MainWindow *_mainWindow;
};

#endif
