/*
 * Copyright 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "util.h"

#include <qfile.h>
#include <qstringlist.h>
#include <qnetworkreply.h>
#include <qprocess.h>

void SignalBucket::saveFile()
{
	_file->open(QIODevice::WriteOnly);
	_file->write(_reply->readAll());
	_file->close();
	_reply->close();
	delete _file;
	_reply->deleteLater();

	QStringList args = _prog.split(' ');
	QProcess::startDetached(args.takeFirst(), args);
}
