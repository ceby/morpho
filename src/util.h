/*
 * Copyright 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef UTIL_H
#define UTIL_H

#include <qobject.h>

class QFile;
class QNetworkReply;
class QString;
class QWidget;

class SignalBucket : public QObject {
	Q_OBJECT
public:
	SignalBucket(QFile *file, QNetworkReply *reply, const QString &prog)
		: _reply(reply)
		, _file(file)
		, _prog(prog)
	{
	}
public slots:
	void saveFile();
private:
	QNetworkReply *_reply;
	QFile *_file;
	QString _prog;
};

#endif
