/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qmainwindow.h>

class QKeyEvent;
class QLineEdit;
class QNetworkReply;
class QNetworkRequest;
class QToolBar;
class QUrl;
class TabBar;
class WebPage;
class WebView;
template<class T> class QLinkedList;

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(const QUrl &url);
	void runCommand(const QString&);
	TabBar *tabs() const { return _tabs; }
	void showCommandOutput(QWidget*);
	void showTabs();
	void openFiles();
	void saveSessionAndClose();
	void toggleInsertMode(int on = -1);
	bool willNotHandleKey(int);
protected slots:
	void updateStatusBar();
	void setCurrent(WebPage*, int);
	void setLinkText(const QString &url, const QString&, const QString&);
	void setStatusBarText(const QString&);
	void changeUrl(const QUrl&);
	void tabCountChanged(int);
	void commandEntered();
protected:
	virtual void keyReleaseEvent(QKeyEvent*);
	void handleKeyReleaseEvent(int);
private:
	void parseStatusStr();

	WebView *_view;
	QToolBar *_statusBar;
	TabBar *_tabs;
	QString _statusBarText, _hoverLink, _url, _currentIndex, _tabCount, _mode;
	QLinkedList<QString*> *_statusTextLeft, *_statusTextRight;
	QLineEdit *_commandLine, *_statusBarLeft, *_statusBarRight;
	QDockWidget *_dock;
	bool _insertMode;

	friend class WebView;
};

#endif
