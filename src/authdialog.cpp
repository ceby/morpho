/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "authdialog.h"

#include <qauthenticator.h>
#include <qdialogbuttonbox.h>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qlineedit.h>

AuthDialog::AuthDialog(QWidget *parent)
	: QDialog(parent)
{
	setWindowTitle("Authentication Required");

	_user = new QLineEdit;
	_password = new QLineEdit;
	_password->setEchoMode(QLineEdit::Password);

	QDialogButtonBox *okayCancel = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(okayCancel, SIGNAL(accepted()),
	        this, SLOT(accept()));
	connect(okayCancel, SIGNAL(rejected()),
	        this, SLOT(reject()));

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(new QLabel(tr("Username:")), 0, 0);
	layout->addWidget(_user, 0, 1);
	layout->addWidget(new QLabel(tr("Password:")), 1, 0);
	layout->addWidget(_password, 1, 1);
	layout->addWidget(okayCancel, 2, 0, 1, 2);
	setLayout(layout);
}

void AuthDialog::authenticate(QAuthenticator *a)
{
	raise();
	activateWindow();
	_user->setFocus();
	if (exec() == QDialog::Accepted) {
		a->setUser(_user->text());
		a->setPassword(_password->text());
	}
}
