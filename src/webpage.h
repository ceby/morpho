/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef WEBPAGE_H
#define WEBPAGE_H

#include <qwebpage.h>

class MainWindow;

class WebPage : public QWebPage {
	Q_OBJECT
public:
	WebPage(QObject *parent = NULL);
	QString titleOrUrl() const;
	void open(const QString&);
protected slots:
	void raiseSelf();
protected:
	virtual QWebPage *createWindow();
	virtual QString userAgentFor(const QUrl&) const;
	friend class MainWindow;
};

#endif
