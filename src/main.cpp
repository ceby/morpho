/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "mainwindow.h"
#include <qapplication.h>
#include <qdesktopservices.h>
#include <qurl.h>
#include "settings.h"
#include "tabbar.h"

#ifdef DBUS
#include "dbus.h"
#include <qdbusconnection.h>
#include <qdbusmessage.h>
#endif

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	QUrl url;
	const QStringList args = app.arguments();
	if (args.count() > 1) {
		url.setUrl(args.at(1));
		if (url.scheme().isEmpty())
			url = QUrl::fromLocalFile(args.at(1));
	}

#ifdef DBUS
	QDBusConnection session = QDBusConnection::sessionBus();
	QDBusMessage msg = QDBusMessage::createMethodCall("org.morpho.browser", "/", "org.morpho.browser", "openUrl");
	QList<QVariant> arg;
	arg << url.toString();
	msg.setArguments(arg);
	if (session.call(msg).type() == QDBusMessage::ReplyMessage)
		return 0;
#endif

	settings = new Settings;

	if (url.isEmpty())
		 url.setUrl(settings->homePage());

	MainWindow window(url);
	settings->setMainWindow(&window);

	QDesktopServices::setUrlHandler("http", window.tabs(), "load");
	QDesktopServices::setUrlHandler("https", window.tabs(), "load");

#ifdef DBUS
	session.registerObject("/", &window);
	session.registerService("org.morpho.browser");
#endif

	window.show();

	return app.exec();
}
