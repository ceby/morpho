/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef SETTINGS_H
#define SETTINGS_H

#include <qobject.h>

class Downloader;
class MainWindow;
class QAuthenticator;
class QIcon;
class QLocalServer;
class QNetworkAccessManager;
class QNetworkReply;
class QSize;
template<class Key, class T> class QHash;

class Settings : public QObject {
	Q_OBJECT
public:
	Settings();
	QString homePage() const { return _homePage; }
	QString downloadPath() const { return _downloadPath; }
	QString downloadProgram() const { return _downloadProgram; }
	QString userAgent() const { return _userAgent; }
	QString statusFormat() const { return _statusFormat; }
	QNetworkAccessManager *networkManager() const { return _networkManager; }
	Downloader *downloader() const { return _downloader; }
	MainWindow *mainWindow() const { return _mainWindow; }
	QString handlerForMime(const QString&) const;
	QString keyword(const QString&) const;
	QString keybind(int keyMask) const;
	void loadDefaultConfig();
	void setMainWindow(MainWindow *v) { _mainWindow = v; }
protected slots:
	void authenticate(QNetworkReply*, QAuthenticator *a);
private:
	void load();
	bool parseFile(const QString&);

	bool _enableJavascript, _enableInspector;
	QString _userAgent, _httpProxy, _downloadPath,
		_downloadProgram, _statusFormat, _homePage;
	QHash<QString, QString> *_keywords, *_mimeHandlers;
	QHash<int, QString> *_keybinds;
	QNetworkAccessManager *_networkManager;
	MainWindow *_mainWindow;
	Downloader *_downloader;
};

extern Settings *settings;

#endif
