/*
 * Copyright 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "downloader.h"

#include "mainwindow.h"
#include <qboxlayout.h>
#include <qdialogbuttonbox.h>
#include <qfiledialog.h>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qnetworkreply.h>
#include <qnetworkrequest.h>
#include <qprocess.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include "settings.h"
#include "util.h"

DownloadDialog::DownloadDialog(QWidget *parent)
	: QDialog(parent)
{
	setWindowTitle(tr("morpho - Open/Download"));
	setAttribute(Qt::WA_DeleteOnClose);

	_handler = new QLineEdit;
	_filePath = new QLineEdit;
	_mimeType = new QLabel;

	QDialogButtonBox *okayCancel = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(okayCancel, SIGNAL(accepted()),
	        this, SLOT(accept()));
	connect(okayCancel, SIGNAL(rejected()),
	        this, SLOT(reject()));

	QGridLayout *runLayout = new QGridLayout;
	runLayout->addWidget(_mimeType, 0, 0, 1, 2);
	runLayout->addWidget(new QLabel(tr("Program:")), 1, 0);
	runLayout->addWidget(_handler, 1, 1);
	QWidget *run = new QWidget;
	run->setLayout(runLayout);

	QPushButton *fileSelector = new QPushButton(tr("Choose File..."));
	fileSelector->setCheckable(false);
	connect(fileSelector, SIGNAL(pressed()),
	        this, SLOT(selectFile()));

	QHBoxLayout *downLayout = new QHBoxLayout;
	downLayout->addWidget(_filePath);
	downLayout->addWidget(fileSelector);
	QWidget *download = new QWidget;
	download->setLayout(downLayout);

	_tabs = new QTabWidget;
	_tabs->addTab(run, tr("Open"));
	_tabs->addTab(download, tr("Save"));

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(_tabs);
	layout->addWidget(okayCancel);
	setLayout(layout);
}

void DownloadDialog::selectFile()
{
	_filePath->setText(QFileDialog::getSaveFileName(parentWidget(), tr("Select Download Location"), _fileName));
}

void DownloadDialog::focusDownload()
{
	_tabs->setCurrentIndex(1);
}

bool DownloadDialog::promptDownload(const QString &type, QString *handler, QString *file)
{
	_fileName = settings->downloadPath() + *file;
	_filePath->setText(_fileName);

	_mimeType->setText(tr("Select program to open file of type %1").arg(type));

	raise();
	activateWindow();
	if (_tabs->currentIndex() == 1)
		_filePath->setFocus();
	else
		_handler->setFocus();

	if (exec() == QDialog::Accepted) {
		if (_tabs->currentIndex() == 1) {
			*file = _filePath->text();
			*handler = "!save";
		} else
			*handler = _handler->text();
		return true;
	} else
		return false;
}

Downloader::Downloader(QObject *parent)
	: QObject(parent)
{
}

inline void openMime(const QString &handler, QNetworkReply *reply, const QString &file)
{
	QNetworkReply *getReply = NULL;

	QString prog = handler;
	prog = prog.replace("@url", reply->url().toString());

	if (prog != handler) {
		QStringList args = prog.split(' ');
		QProcess::startDetached(args.takeFirst(), args);
		return;
	}

	getReply = settings->networkManager()->get(reply->request());
	QFile *out = new QFile((settings->downloadPath().isEmpty() ? QDir::tempPath() :
		settings->downloadPath()) + file);

	int i;
	if ((i = prog.indexOf("@file")) != -1)
		prog.replace(i, 5, out->fileName());
	else
		prog += ' ' + out->fileName();

	SignalBucket *sb = new SignalBucket(out, getReply, prog);
	QObject::connect(getReply, SIGNAL(finished()),
	                 sb, SLOT(saveFile()));
}

inline void startDownload(const QString &url, const QString &file)
{
	QStringList args = settings->downloadProgram()
		.replace("@url", url).replace("@file", file).split(' ');
	QProcess::startDetached(args.takeFirst(), args);
}

void Downloader::downloadPrompt(QNetworkReply *reply)
{
	bool dl = false;
	if (!reply) {
		reply = static_cast<QNetworkReply*>(sender());
		dl = true;
	}

	QString handler, contentType;
	contentType = reply->header(QNetworkRequest::ContentTypeHeader).toString();
	handler = settings->handlerForMime(contentType);

	QString url = reply->url().toString();
	QString file = url.section('/', -1);

	if (handler == "!save" && !settings->downloadPath().isEmpty())
		file.prepend(settings->downloadPath());
	else if (handler.isEmpty() || handler == "!save" || dl) {
		DownloadDialog *d = new DownloadDialog(settings->mainWindow());

		if (dl)
			d->focusDownload();

		if (!d->promptDownload(contentType, &handler, &file))
			goto done; // the download was cancelled
	}

	if (handler == "!save")
		startDownload(url, file);
	else
		openMime(handler, reply, file);

done:
	reply->deleteLater();
}

void Downloader::download(const QNetworkRequest &req)
{
	QNetworkReply *reply = settings->networkManager()->head(req);
	connect(reply, SIGNAL(finished()),
	        this, SLOT(downloadPrompt()));
}
