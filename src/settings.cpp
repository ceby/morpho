/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "settings.h"

#include "authdialog.h"
#include "defaultconfig.h"
#include "downloader.h"
#include "mainwindow.h"
#include <qdir.h>
#include <qfile.h>
#include <qhash.h>
#include <qlocale.h>
#include <qmessagebox.h>
#include <qnetworkaccessmanager.h>
#include <qnetworkproxy.h>
#include <qurl.h>
#include <qwebsettings.h>

#ifdef Q_WS_WIN
#include <windows.h>

QString windowsVersion()
{
	OSVERSIONINFOEX version;
	version.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	GetVersionEx((OSVERSIONINFO*)&version);

	return (version.dwPlatformId == 2 ? "Windows NT " : "Windows ")
		+ QString::number(version.dwMajorVersion) + '.' + QString::number(version.dwMinorVersion);
}

#define PLATFORM "Windows"
#define OS + windowsVersion() +
#endif

#ifdef Q_WS_X11
#define PLATFORM "X11"
#endif

Settings *settings;

Settings::Settings()
	: _networkManager(new QNetworkAccessManager(this))
	, _downloader(new Downloader(this))
{
	QWebSettings::globalSettings()->setIconDatabasePath(QString("%1/.config/morpho/").arg(QDir::homePath()));

	connect(_networkManager, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)),
	        this, SLOT(authenticate(QNetworkReply*, QAuthenticator*)));

	load();

	// TODO: don't hardcode WebKit version
	QString locale = QLocale::system().name().replace("_", "-");
	_userAgent = "Mozilla/5.0 (" PLATFORM "; U; " OS "; " + (locale == "C" ? "en" : locale) +
		") AppleWebKit/525.5+ (KHTML, like Gecko) morpho/" MORPHO_VERSION;
}

bool toBool(const QString &str)
{
	return str == "true" || str == "on";
}

int keyStrToMask(const QString &str)
{
	QKeySequence seq(str);
	int mask = 0;
	for (unsigned i = seq.count(); i--;)
		mask += seq[i];
	return mask;
}

bool Settings::parseFile(const QString &fileName)
{
	QFile file(fileName);

	if (!file.exists())
		return false;

	file.open(QIODevice::ReadOnly);
	QTextStream f(&file);

	while (!f.atEnd()) {
		QString line = f.readLine();
		if (line.isEmpty() || line[0] == '#')
			continue;

		QString fullKey = line.section('=', 0, 0);
		QString v = line.section('=', 1).trimmed();

		QStringList keys = fullKey.split('.');
		QString key = keys[0].trimmed();

		if (key == "javascript")
			_enableJavascript = toBool(v);

		else if (key == "inspector")
			_enableInspector = toBool(v);

		else if (key == "homepage")
			_homePage = v;

		else if (key == "http_proxy")
			_httpProxy = v;

		else if (key == "mime_handlers")
			if (v.isEmpty())
				_mimeHandlers->remove(keys[1].trimmed());
			else
				_mimeHandlers->insert(keys[1].trimmed(), v);

		else if (key == "download_location")
			_downloadPath = v;

		else if (key == "status_format")
			_statusFormat = v;

		else if (key == "download_program")
			_downloadProgram = v;

		else if (key == "search_words")
			if (v.isEmpty())
				_keywords->remove(keys[1].trimmed());
			else
				_keywords->insert(keys[1].trimmed(), v);

		else if (key == "key")
			if (v.isEmpty())
				_keybinds->remove(keyStrToMask(keys[1].trimmed()));
			else
				_keybinds->insert(keyStrToMask(keys[1].trimmed()), v);
	}

	file.close();
	return true;
}

void Settings::loadDefaultConfig()
{
	_enableJavascript = true;
	_enableInspector = true;
	_homePage = "http://www.google.com/";
	_downloadPath = QString();
	_downloadProgram = QString();
	_httpProxy = QString();
	_statusFormat = "%U %> %M [%t/%T]";

	_keywords = new QHash<QString, QString>;
	_mimeHandlers = new QHash<QString, QString>;

	_keybinds = new QHash<int, QString>;
	_keybinds->insert(Qt::AltModifier + Qt::Key_Left, "back");
	_keybinds->insert(Qt::AltModifier + Qt::Key_Right, "forward");
	_keybinds->insert(Qt::ControlModifier + Qt::Key_W, "tabclose");
	_keybinds->insert(Qt::ControlModifier + Qt::Key_T, "tabadd");
	_keybinds->insert(Qt::ControlModifier + Qt::Key_I, "ls");
	_keybinds->insert(Qt::ControlModifier + Qt::Key_R, "reload");
	_keybinds->insert(Qt::ControlModifier + Qt::Key_Q, "quit");
	_keybinds->insert(Qt::Key_I, "insert");
	_keybinds->insert(Qt::Key_O, "%open");
	_keybinds->insert(Qt::Key_T, "%tabopen");
	_keybinds->insert(Qt::Key_D, "tabclose");
	_keybinds->insert(Qt::Key_B, "%buffer");
}

void Settings::load()
{
	loadDefaultConfig();

	bool noConfigFiles = true;

	QString globalConfig = qgetenv("XDG_CONFIG_DIRS");
	if (globalConfig.isEmpty())
		globalConfig = "/etc/xdg";
	foreach(QString file, globalConfig.split(':'))
		if (parseFile(file + "/morpho/settings.conf"))
			noConfigFiles = false;

	QString userConfig = qgetenv("XDG_CONFIG_DIR");
	if (userConfig.isEmpty())
		userConfig = QDir::homePath() + "/.config/morpho/settings.conf";

	if (parseFile(userConfig))
		noConfigFiles = false;

	if (noConfigFiles) {
		if (!QFile::exists(QDir::homePath() + "/.config/morpho"))
			QDir::home().mkpath(".config/morpho");

		QFile f(userConfig);
		if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
			QMessageBox::warning(NULL, "morpho", tr("Could not write default configuration file to '%1'").arg(userConfig));
		else {
			f.write(defaultConfig, sizeof(defaultConfig) - 1);
			f.close();
		}
	}

	QWebSettings *s = QWebSettings::globalSettings();
	s->setAttribute(QWebSettings::DeveloperExtrasEnabled, _enableInspector);
	s->setAttribute(QWebSettings::JavascriptEnabled, _enableJavascript);

	if (_httpProxy == "!env")
		_httpProxy = qgetenv("http_proxy");

	if (!_httpProxy.isEmpty()) {
		QUrl proxy(_httpProxy);
		networkManager()->setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,
			proxy.host(), proxy.port(8080), proxy.userName(), proxy.password()));
	}

	// TODO: FTP, etc...

	if (QDir::isRelativePath(_downloadPath))
		_downloadPath.prepend(QDir::homePath() + '/');
	_downloadPath.append('/');

	if (!_downloadPath.isEmpty() && !QFile::exists(_downloadPath)) {
		QMessageBox::warning(NULL, "morpho", tr("The specified download path, '%1', does not exist. It will not be used.").arg(_downloadPath));
		_downloadPath.clear();
	}

	_keybinds->squeeze();
	_keywords->squeeze();
	_mimeHandlers->squeeze();
}

QString Settings::handlerForMime(const QString &type) const
{
	return _mimeHandlers->value(type.split(';')[0].trimmed());
}

QString Settings::keyword(const QString &key) const
{
	return _keywords->value(key);
}

QString Settings::keybind(int keyMask) const
{
	return _keybinds->value(keyMask);
}

void Settings::authenticate(QNetworkReply*, QAuthenticator *a)
{
	AuthDialog *dialog = new AuthDialog(_mainWindow);
	dialog->authenticate(a);
	delete dialog;
}
