/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <qdialog.h>

class QAuthenticator;
class QLineEdit;

class AuthDialog : public QDialog {
	Q_OBJECT
public:
	AuthDialog(QWidget *parent);
	void authenticate(QAuthenticator *a);
private:
	QLineEdit *_user, *_password;
};

#endif
