/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#include "webpage.h"

#include <qwebframe.h>
#include "settings.h"
#include "tabbar.h"

WebPage::WebPage(QObject *parent)
	: QWebPage(parent)
{
}

void WebPage::open(const QString &url)
{
	if (url.isEmpty()) {
		mainFrame()->load(QUrl(("about:blank")));
		return;
	}

	int keyLen = url.indexOf(' ');

	if (keyLen != -1) {
		QString v = ::settings->keyword(url.left(keyLen).toLower());
		if (v != QString()) {
			mainFrame()->load(QUrl(v.replace("%s", url.mid(keyLen+1).replace(' ', '+'))));
			return;
		}
	}

	QUrl u(url);
	if (u.scheme().isEmpty())
		mainFrame()->load(QUrl("http://" + url));
	else
		mainFrame()->load(u);
}

QString WebPage::titleOrUrl() const
{
	if (mainFrame()->title().isEmpty())
		return mainFrame()->url().toString();
	else
		return mainFrame()->title();
}

QWebPage *WebPage::createWindow()
{
	// TODO: implement background/foreground tabs
	return static_cast<TabBar*>(parent())->addTab();
}

QString WebPage::userAgentFor(const QUrl&) const
{
	return ::settings->userAgent();
}

void WebPage::raiseSelf()
{
	static_cast<TabBar*>(parent())->setCurrent(this);
}
