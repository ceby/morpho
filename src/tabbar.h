/*
 * Copyright 2007, 2008 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Morpho.
 *
 * Morpho is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */ 

#ifndef TABBAR_H
#define TABBAR_H

#include <qobject.h>

class QMenu;
class QUrl;
class WebPage;
template<class T> class QList;
template<class T> class QStack;

class TabBar : public QObject {
	Q_OBJECT
public:
	TabBar(QObject *parent = NULL);

	WebPage* currentTab() const { return _currentTab; }
	int currentIndex() const { return _currentIndex; }
	QList<WebPage*> *tabs() const { return _tabs; }

	void setCurrentIndex(int);
	void setCurrent(WebPage *tab, bool calledWhileRemoving = false);

	void load(const QUrl&);

	WebPage *addTab(bool focus = true);
	WebPage *addTab(const QString &url, bool focus = true);
	void removeTab(WebPage*);
	void removeTab();
signals:
	void currentChanged(WebPage*, int);
	void tabCountChanged(int);
protected slots:
	void respondToAction();
private:
	void setCurrent(WebPage*, int, bool);

	QList<WebPage*> *_tabs;
	WebPage *_currentTab;
	int _currentIndex;
	QStack<WebPage*> *_tabStack;
	QMenu *_trashMenu;
};

#endif
